FROM debian:stable-slim


# setup first
RUN echo 'deb http://httpredir.debian.org/debian testing main contrib non-free' > /etc/apt/sources.list
RUN apt update && apt install -y -t testing libstdc++6

RUN mkdir /rage_server
WORKDIR /rage_server
COPY . /rage_server


# RAGE Multiplayer
## Installing / Getting started
### Prerequisites
    Client-Side: Javascript
    Server-Side: Nods.js
    Database: null
### Installing
1.  Copy all file to execute server
2.  Copy `.env` from `.env.example`
    ```bash
    cp .env.example .env
    ```
3. Install dependency
    ```bash
    npm install
    ```
4. Change modification of ```server``` file
    ```bash
    chmod +x ./server
    ```

## Running Server
Run server with command:
```bash
./server
```
## Extra for Docker
### Build image
```
docker build -t rage-mp:v1 .
```

### Run container from image
```
docker run -it --name rage-mp -p 22005:22005 -p 22005:22005/udp -p 22006:22006 rage-mp:v1
```

### Start server
```
docker start rage-mp
```

### Stop server
```
docker stop rage-mp
```

### Inspect server
```
docker exec -it rage-mp /bin/bash
```

### Docker compose
```
docker-compose run --rm --service-ports rage-mp /bin/bash
```

